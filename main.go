package main

import (
	"gitlab.com/nerginturgut/scan-service/cmd"
)

func main() {
	cmd.Execute()
}
