package lib

import (
	"context"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
	"time"
)

func NewDatabase(uri, databaseName string) (db *mongo.Database, err error) {
	client, err := mongo.NewClient(options.Client().ApplyURI(uri))

	if err != nil {
		return db, err
	}

	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	err = client.Connect(ctx)
	if err != nil {
		log.Printf("Mongo: mongo client couldn't connect with background context: %v", err)
		return db, err
	}

	// Check the connection
	err = client.Ping(ctx, nil)
	if err != nil {
		return db, err
	}

	db = client.Database(databaseName)

	return db, err
}
