package cmd

import (
	"fmt"
	"github.com/labstack/echo/v4"
	"github.com/spf13/cobra"
	echoSwagger "github.com/swaggo/echo-swagger"
	"gitlab.com/nerginturgut/scan-service/cmd/api"
	_ "gitlab.com/nerginturgut/scan-service/docs"
	"log"
)

var apiCmd = &cobra.Command{
	Use: "api-initialize",
}

// @title Scan API
// @version 1.0
// @description Scan API
// @license.name Apache 2.0
// @license.url http://www.apache.org/licenses/LICENSE-2.0.html
// @BasePath /
// @schemes http
func init() {
	var port string

	rootCmd.AddCommand(apiCmd)
	//flags
	apiCmd.PersistentFlags().StringVarP(&port, "port", "p", "5001", "Service Port")

	apiCmd.RunE = func(cmd *cobra.Command, args []string) error {

		instance := echo.New()

		instance.GET("/swagger/*", echoSwagger.WrapHandler)

		api.RegisterHandlers(instance, globalConfig)

		if err := instance.Start(fmt.Sprintf("0.0.0.0:%s", port)); err != nil {
			log.Fatalf("failed to gracefully shutting down. %s", err.Error())
		}

		return nil
	}
}
