package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
	"gitlab.com/nerginturgut/scan-service/internal/config"
	"log"
	"os"
)

var (
	Version      string
	globalConfig = &config.Configuration{}
	rootCmd      = &cobra.Command{
		Use: "config-initialize",
	}
)

func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func init() {
	var cfgFile string

	cobra.OnInitialize()

	rootCmd.PersistentFlags().StringVarP(&cfgFile, "config", "c", "newScanProject.test.yaml", "Service configuration file path")

	rootCmd.PersistentPreRunE = func(cmd *cobra.Command, args []string) (err error) {
		if len(args) == 0 && len(cmd.Use) == 0 {
			cmd.Help()
			return
		}
		configFile := fmt.Sprintf("./config/%s", cfgFile)
		if globalConfig, err = config.GetAllValues(configFile); err != nil {
			log.Fatalf("Failed to read config file. %v", err.Error())
		}
		fmt.Println(globalConfig)
		return
	}
}
