package api

import (
	"fmt"
	"github.com/labstack/echo/v4"
	"gitlab.com/nerginturgut/scan-service/internal/config"
	"gitlab.com/nerginturgut/scan-service/internal/scan"
	scanRepository "gitlab.com/nerginturgut/scan-service/internal/scan/mongo"
	"gitlab.com/nerginturgut/scan-service/lib"
)

func RegisterHandlers(instance *echo.Echo, config *config.Configuration) {
	mongoClient, err := lib.NewDatabase(config.Mongo.Uri, config.Mongo.Db)
	if err != nil {
		fmt.Printf("Cannot connect to MongoDb")
		panic(err)
	}

	scanRepository := scanRepository.NewRepository(mongoClient, config)
	scanService := scan.NewService(scanRepository)
	scanController := scan.NewController(scanService)

	instance.POST("/scan", scanController.AddNewScan)
	instance.GET("/scan/:id", scanController.GetScanById)

}
