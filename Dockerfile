FROM golang:1.17-alpine

RUN apk add --no-cache python3 py3-pip && \
    pip3 install bandit

WORKDIR /go/src/app

COPY go.mod ./
RUN go mod download
COPY . .

RUN go build -o main .

ENTRYPOINT ["./main"]
CMD  ["api-initialize"]
