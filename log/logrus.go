package log

import (
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"go.elastic.co/ecslogrus"
)

type Log struct {
	Logs *logrus.Logger
}

func New() {

}

func (l Log) SetFormat() {
	l.Logs.SetFormatter(&ecslogrus.Formatter{})
}
func (l Log) SetDecr(err error) {
	l.Logs.WithError(errors.New("boom!")).WithField("custom", "foo").Info("hello")
}
