package config

import (
	"errors"
	"fmt"
	"github.com/spf13/viper"
)

var (
	cfgReader *configReader
)

type (
	Configuration struct {
		Mongo MongoSettings
	}
	MongoSettings struct {
		Uri            string
		Db             string
		CollectionName string
	}
	configReader struct {
		configFile string
		v          *viper.Viper
	}
)

func GetAllValues(configFile string) (configuration *Configuration, err error) {

	newConfigReader(configFile)
	if err = cfgReader.v.ReadInConfig(); err != nil {
		return nil, errors.New(fmt.Sprintf("Failed to read config file. %s", err.Error()))
	}
	err = cfgReader.v.Unmarshal(&configuration)
	if err != nil {
		return nil, errors.New("Config: Failed to unmarshal yaml file to configuration object")
	}
	return
}

func newConfigReader(configFile string) {

	vip := viper.GetViper()
	vip.SetConfigType("yaml")
	vip.SetConfigFile(configFile)
	cfgReader = &configReader{
		configFile: configFile,
		v:          vip,
	}
}
