package errors

import "github.com/labstack/echo/v4"

type Error struct {
	ApplicationName string `json:"applicationName"`
	Operation       string `json:"operation"`
	Description     string `json:"description"`
	StatusCode      int    `json:"statusCode"`
	ErrorCode       int    `json:"errorCode"`
}

func New(applicationName, operation, description string, statusCode, errorCode int) *Error {
	return &Error{
		ApplicationName: applicationName,
		Operation:       operation,
		Description:     description,
		StatusCode:      statusCode,
		ErrorCode:       errorCode,
	}
}

func (e *Error) WrapOperation(operation string) *Error {
	e.Operation = operation
	return e
}

func (e *Error) ToResponse(ctx echo.Context) error {
	return ctx.JSON(e.StatusCode, e)
}

func (e *Error) WrapDesc(desc string) *Error {
	e.Description = desc
	return e
}

func (e *Error) WrapErrorCode(errorCode int) *Error {
	e.ErrorCode = errorCode
	return e
}

var (
	UnknownError    = New("scan", "service", "unknown errors", 500, 0)
	ValidationError = New("scan", "service", "validation errors", 400, 0)
	NotFound        = New("scan", "service", "not found scan", 404, 0)
)
