package scan

import (
	"github.com/google/uuid"
	"github.com/labstack/echo/v4"
	"gitlab.com/nerginturgut/scan-service/internal/errors"
	"net/http"
)

type controller struct {
	scanService *scanService
}

func NewController(scanService *scanService) *controller {
	return &controller{
		scanService: scanService,
	}
}

// @Summary Post NewScan
// @Description Post NewScan
// @Tags Post Scan V1
// @Accept json
// @Produce json
// @Param content body ScanRequest true "Add New Scan"
// @Success 200 {object} string
// @Router /scan [post]
func (r *controller) AddNewScan(ctx echo.Context) error {

	req := new(ScanRequest)

	if err := ctx.Bind(req); err != nil {
		return errors.ValidationError.WrapOperation("controller").WrapErrorCode(0001).WrapDesc(err.Error()).ToResponse(ctx)
	}

	id, err := r.scanService.AddNewScan(ctx.Request().Context(), req)
	if err != nil {
		return err.ToResponse(ctx)
	}
	return ctx.JSON(http.StatusOK, id)
}

// @Summary Get Scan By Id
// @Description Get Scan By Id
// @Tags Get Scan V1
// @Accept json
// @Produce json
// @Param id path string true "scan id"
// @Success 200 {object} Scan
// @Router /scan/{id} [get]
func (r *controller) GetScanById(ctx echo.Context) error {
	id := ctx.Param("id")
	if _, err := uuid.Parse(id); err != nil {
		return errors.ValidationError.WrapOperation("controller").WrapErrorCode(0001).WrapDesc(err.Error()).ToResponse(ctx)
	}

	result, err := r.scanService.GetScanById(ctx.Request().Context(), id)
	if err != nil {
		return err.ToResponse(ctx)
	}
	return ctx.JSON(http.StatusOK, result)
}
