package scan

import "time"

type ScanRequest struct {
	Url string `json:"url"`
}

type SearchRequest struct {
	Sort                []string            `json:"sort"`
	ExactFilters        map[string][]string `json:"exactFilters"`
	PrefixFilters       map[string][]string `json:"prefixFilters"`
	SelectedFields      []string            `json:"selectedFields"`
	NotIncludingFilters map[string][]string `json:"notIncludingFilters"`
	Skip                int64               `json:"skip"`
	Limit               int64               `json:"limit"`
}

type BanditError struct {
	Filename string `json:"filename"`
	Reason   string `json:"reason"`
}
type IssueCwe struct {
	Id   int    `json:"id"`
	Link string `json:"link"`
}

type BanditResult struct {
	EndColOffset    int      `json:"end_col_offset"`
	Filename        string   `json:"filename"`
	IssueCwe        IssueCwe `json:"issue_cwe"`
	LineRange       []int    `json:"line_range"`
	MoreInfo        string   `json:"more_info"`
	Code            string   `json:"code"`
	ColOffset       int      `json:"col_offset"`
	IssueText       string   `json:"issue_text"`
	LineNumber      int      `json:"line_number"`
	TestId          string   `json:"test_id"`
	TestName        string   `json:"test_name"`
	IssueConfidence string   `json:"issue_confidence"`
	IssueSeverity   string   `json:"issue_severity"`
}
type BanditMetric struct {
	CONFIDENCEMEDIUM    int `json:"CONFIDENCE.MEDIUM"`
	SEVERITYHIGH        int `json:"SEVERITY.HIGH"`
	SEVERITYLOW         int `json:"SEVERITY.LOW"`
	SEVERITYUNDEFINED   int `json:"SEVERITY.UNDEFINED"`
	Nosec               int `json:"nosec"`
	CONFIDENCEHIGH      int `json:"CONFIDENCE.HIGH"`
	CONFIDENCELOW       int `json:"CONFIDENCE.LOW"`
	CONFIDENCEUNDEFINED int `json:"CONFIDENCE.UNDEFINED"`
	SEVERITYMEDIUM      int `json:"SEVERITY.MEDIUM"`
	Loc                 int `json:"loc"`
	SkippedTests        int `json:"skipped_tests"`
}

type Scan struct {
	Id               string                  `json:"_id,omitempty"`
	SecurityCriteria string                  `json:"securityCriteria"`
	Errors           []BanditError           `json:"errors"`
	GeneratedAt      time.Time               `json:"generated_at"`
	Results          []BanditResult          `json:"results"`
	Metrics          map[string]BanditMetric `json:"metrics"`
}
