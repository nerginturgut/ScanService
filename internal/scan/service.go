package scan

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/google/uuid"
	"gitlab.com/nerginturgut/scan-service/internal/errors"
	"gitlab.com/nerginturgut/scan-service/internal/scan/mongo"
	git "gopkg.in/src-d/go-git.v4"
	"os"
	"os/exec"
	"strings"
)

type scanService struct {
	scanRepository *mongo.ScanRepository
}

func NewService(scanRepository *mongo.ScanRepository) *scanService {
	return &scanService{scanRepository: scanRepository}
}

func (r *scanService) AddNewScan(c context.Context, req *ScanRequest) (interface{}, *errors.Error) {
	dir := fmt.Sprintf("/tmp/src/%s", uuid.New().String())

	repo, err := git.PlainClone(dir, false, &git.CloneOptions{
		URL: req.Url,
	})

	if err != nil {
		return nil, errors.UnknownError.WrapErrorCode(0001).WrapDesc(err.Error())
	}

	if repo == nil {
		return nil, errors.UnknownError.WrapErrorCode(0002).WrapDesc("Git clone process is failed!")
	}

	filePath := fmt.Sprintf("%s/scan-result.json", dir)
	cmd := exec.Command("bandit", "-r", dir, "--format", "json", "--output", filePath)

	err = cmd.Start()
	defer os.RemoveAll(dir)

	if err != nil {
		return nil, errors.UnknownError.WrapErrorCode(0003).WrapDesc(fmt.Sprintf("Error starting bandit: %s", err.Error()))
	}

	err = cmd.Wait()
	data, err := os.ReadFile(filePath)
	result := string(data)
	if err != nil {
		return nil, errors.UnknownError.WrapErrorCode(0004).WrapDesc(fmt.Sprintf("Error reading file: %s", err))
	}

	if err != nil {
		errorMessage := ""
		//cast to exit error
		exitErr, ok := err.(*exec.ExitError)
		if ok {
			errorMessage = fmt.Sprintf("Command failed with exit code %d: %s\n", exitErr.ExitCode(), result)
		} else {
			errorMessage = fmt.Sprintf(fmt.Sprintf("Error running bandit: %s", err.Error()))
		}
		return nil, errors.UnknownError.WrapErrorCode(0004).WrapDesc(errorMessage)
	}

	scanResult := Scan{}

	err = json.NewDecoder(strings.NewReader(result)).Decode(&scanResult)
	if err != nil {
		return nil, errors.ValidationError.WrapErrorCode(0005).
			WrapDesc(fmt.Sprintf("Error decoding JSON output: %s", err.Error()))
	}

	if len(scanResult.Errors) > 0 {
		error := r.toMetricSingleError(scanResult.Errors)
		return nil, errors.UnknownError.WrapDesc(error)

	}

	id, err := r.scanRepository.Insert(c, r.toBanditScanModel(scanResult))
	if err != nil {
		return nil, errors.UnknownError.
			WrapErrorCode(0006).
			WrapOperation("repository").
			WrapDesc(fmt.Sprintf("ScanModel Insert error: %s", err.Error()))
	}

	return id, nil
}

func (r *scanService) GetScanById(ctx context.Context, scanId string) (*Scan, *errors.Error) {
	result, err := r.scanRepository.GetById(ctx, scanId)
	if err != nil {
		return nil, errors.UnknownError.
			WrapOperation("repository").
			WrapErrorCode(0007).
			WrapDesc(fmt.Sprintf("Get ScanModel error: %s", err.Error()))

	}
	return r.toBanditScanResponse(result), nil
}
