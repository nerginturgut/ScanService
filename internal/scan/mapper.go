package scan

import (
	"fmt"
	"github.com/google/uuid"
	"gitlab.com/nerginturgut/scan-service/internal/scan/mongo"
)

const TOTALMETRICKEY = "_totals"

func convertMetricsToResponse(metrics map[string]mongo.BanditMetric) map[string]BanditMetric {
	banditMetrics := make(map[string]BanditMetric, 0)
	for key, value := range metrics {
		banditMetrics[key] = BanditMetric{
			CONFIDENCEMEDIUM:    value.CONFIDENCEMEDIUM,
			SEVERITYHIGH:        value.SEVERITYHIGH,
			SEVERITYLOW:         value.SEVERITYLOW,
			SEVERITYUNDEFINED:   value.SEVERITYUNDEFINED,
			Nosec:               value.Nosec,
			CONFIDENCEHIGH:      value.CONFIDENCEHIGH,
			CONFIDENCELOW:       value.CONFIDENCELOW,
			CONFIDENCEUNDEFINED: value.CONFIDENCEUNDEFINED,
			SEVERITYMEDIUM:      value.SEVERITYMEDIUM,
			Loc:                 value.Loc,
			SkippedTests:        value.SkippedTests,
		}
	}
	return banditMetrics
}

func convertMetricsToModel(metrics map[string]BanditMetric) map[string]mongo.BanditMetric {
	banditMetrics := make(map[string]mongo.BanditMetric, 0)
	for key, value := range metrics {
		banditMetrics[key] = mongo.BanditMetric{
			CONFIDENCEMEDIUM:    value.CONFIDENCEMEDIUM,
			SEVERITYHIGH:        value.SEVERITYHIGH,
			SEVERITYLOW:         value.SEVERITYLOW,
			SEVERITYUNDEFINED:   value.SEVERITYUNDEFINED,
			Nosec:               value.Nosec,
			CONFIDENCEHIGH:      value.CONFIDENCEHIGH,
			CONFIDENCELOW:       value.CONFIDENCELOW,
			CONFIDENCEUNDEFINED: value.CONFIDENCEUNDEFINED,
			SEVERITYMEDIUM:      value.SEVERITYMEDIUM,
			Loc:                 value.Loc,
			SkippedTests:        value.SkippedTests,
		}
	}
	return banditMetrics
}

func convertBanditResultToModel(results []BanditResult) []mongo.BanditResult {
	banditResult := make([]mongo.BanditResult, len(results))
	for key, value := range results {
		banditResult[key] = mongo.BanditResult{
			EndColOffset: value.EndColOffset,
			Filename:     value.Filename,
			IssueCwe: mongo.IssueCwe{
				Id:   value.IssueCwe.Id,
				Link: value.IssueCwe.Link,
			},
			LineRange:       value.LineRange,
			MoreInfo:        value.MoreInfo,
			Code:            value.Code,
			ColOffset:       value.ColOffset,
			IssueText:       value.IssueText,
			LineNumber:      value.LineNumber,
			TestId:          value.TestId,
			TestName:        value.TestName,
			IssueConfidence: value.IssueConfidence,
			IssueSeverity:   value.IssueSeverity,
		}
	}
	return banditResult
}

func convertBanditResultToResponse(results []mongo.BanditResult) []BanditResult {
	banditResult := make([]BanditResult, len(results))
	for key, value := range results {
		banditResult[key] = BanditResult{
			EndColOffset: value.EndColOffset,
			Filename:     value.Filename,
			IssueCwe: IssueCwe{
				Id:   value.IssueCwe.Id,
				Link: value.IssueCwe.Link,
			},
			LineRange:       value.LineRange,
			MoreInfo:        value.MoreInfo,
			Code:            value.Code,
			ColOffset:       value.ColOffset,
			IssueText:       value.IssueText,
			LineNumber:      value.LineNumber,
			TestId:          value.TestId,
			TestName:        value.TestName,
			IssueConfidence: value.IssueConfidence,
			IssueSeverity:   value.IssueSeverity,
		}
	}
	return banditResult
}

func (r *scanService) toBanditScanModel(scan Scan) *mongo.ScanModel {
	return &mongo.ScanModel{
		Id:               uuid.New().String(),
		SecurityCriteria: r.getSecurityCriteria(scan.Metrics[TOTALMETRICKEY]),
		Errors:           convertBanditErrorToModel(scan.Errors),
		GeneratedAt:      scan.GeneratedAt,
		Results:          convertBanditResultToModel(scan.Results),
		Metrics:          convertMetricsToModel(scan.Metrics),
	}

}

func convertBanditErrorToResponse(errors []mongo.BanditError) []BanditError {
	banditErrors := make([]BanditError, len(errors))
	for key, value := range errors {
		banditErrors[key] = BanditError{
			Filename: value.Filename,
			Reason:   value.Reason,
		}
	}
	return banditErrors
}

func convertBanditErrorToModel(metrics []BanditError) []mongo.BanditError {
	banditErrors := make([]mongo.BanditError, len(metrics))
	for key, value := range metrics {
		banditErrors[key] = mongo.BanditError{
			Filename: value.Filename,
			Reason:   value.Reason,
		}
	}
	return banditErrors
}

func (r *scanService) toBanditScanResponse(scan *mongo.ScanModel) *Scan {
	return &Scan{
		Id:               scan.Id,
		Errors:           convertBanditErrorToResponse(scan.Errors),
		GeneratedAt:      scan.GeneratedAt,
		Results:          convertBanditResultToResponse(scan.Results),
		Metrics:          convertMetricsToResponse(scan.Metrics),
		SecurityCriteria: scan.SecurityCriteria,
	}

}

func (r *scanService) getSecurityCriteria(metric BanditMetric) string {
	if metric.SEVERITYHIGH <= 1 {
		return "POSITIVE"
	}
	return "NEGATIVE"
}

func (r *scanService) toMetricSingleError(banditError []BanditError) string {
	str := ""
	for _, value := range banditError {
		str += fmt.Sprintf("%s:%s, ", value.Filename, value.Reason)
	}
	return str
}
