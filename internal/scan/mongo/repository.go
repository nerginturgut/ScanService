package mongo

import (
	"context"
	"gitlab.com/nerginturgut/scan-service/internal/config"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

type ScanRepository struct {
	db     *mongo.Collection
	config config.Configuration
}

func NewRepository(db *mongo.Database, c *config.Configuration) *ScanRepository {
	return &ScanRepository{db: db.Collection(c.Mongo.CollectionName)}
}

func (r *ScanRepository) GetById(ctx context.Context, id string) (n *ScanModel, err error) {
	err = r.db.FindOne(ctx, bson.M{"_id": id}).Decode(&n)
	return n, err
}

func (r *ScanRepository) Insert(ctx context.Context, scan *ScanModel) (n interface{}, err error) {
	result, err := r.db.InsertOne(ctx, &scan)
	return result.InsertedID, err
}
