package mongo

import "time"

type BanditError struct {
	Filename string `bson:"filename"`
	Reason   string `bson:"reason"`
}
type IssueCwe struct {
	Id   int    `bson:"id"`
	Link string `bson:"link"`
}

type BanditResult struct {
	EndColOffset    int      `bson:"end_col_offset"`
	Filename        string   `bson:"filename"`
	IssueCwe        IssueCwe `bson:"issue_cwe"`
	LineRange       []int    `bson:"line_range"`
	MoreInfo        string   `bson:"more_info"`
	Code            string   `bson:"code"`
	ColOffset       int      `bson:"col_offset"`
	IssueText       string   `bson:"issue_text"`
	LineNumber      int      `bson:"line_number"`
	TestId          string   `bson:"test_id"`
	TestName        string   `bson:"test_name"`
	IssueConfidence string   `bson:"issue_confidence"`
	IssueSeverity   string   `bson:"issue_severity"`
}
type BanditMetric struct {
	CONFIDENCEMEDIUM    int `bson:"CONFIDENCE.MEDIUM"`
	SEVERITYHIGH        int `bson:"SEVERITY.HIGH"`
	SEVERITYLOW         int `bson:"SEVERITY.LOW"`
	SEVERITYUNDEFINED   int `bson:"SEVERITY.UNDEFINED"`
	Nosec               int `bson:"nosec"`
	CONFIDENCEHIGH      int `bson:"CONFIDENCE.HIGH"`
	CONFIDENCELOW       int `bson:"CONFIDENCE.LOW"`
	CONFIDENCEUNDEFINED int `bson:"CONFIDENCE.UNDEFINED"`
	SEVERITYMEDIUM      int `bson:"SEVERITY.MEDIUM"`
	Loc                 int `bson:"loc"`
	SkippedTests        int `bson:"skipped_tests"`
}

type ScanModel struct {
	Id               string                  `bson:"_id"`
	SecurityCriteria string                  `bson:"securityCriteria"`
	Errors           []BanditError           `bson:"errors"`
	GeneratedAt      time.Time               `bson:"generated_at"`
	Results          []BanditResult          `bson:"results"`
	Metrics          map[string]BanditMetric `bson:"metrics"`
}
